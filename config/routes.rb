Rails.application.routes.draw do
  # get 'book_ads/new'
  # get 'books/new'
  root 'homepage#welcome_screen'

  get 'login', to: 'sessions#new'
  get 'logout', to: 'sessions#destroy'
  get 'sign_up', to: 'users#new'

  get 'confirm_email', to: 'users#confirm_email'
  get 'confirmation_page', to: 'users#confirmation_page'
  # get 'welcome_screen', to: 'homepage#welcome_screen'
  get 'user_dashboard', to: 'dashboard#user_dashboard'
  
  get 'message_window_new', to: 'dashboard#message_window_new'
  # post 'advanced_search_link', to: 'dashboard#advanced_search_link'
  post 'advanced_search_results', to: 'dashboard#advanced_search_results'
  get 'advanced_search_results', to: 'dashboard#advanced_search_results'
  get 'unread_message_count', to: 'dashboard#unread_message_count'
  # get 'edit_profile', to: 'dashboard#edit_profile_page'
  # post 'update_profile', to: 'dashboard#update_profile'

  get 'mark_it_read_host', to: 'messages#mark_it_read_host'
  get 'mark_it_read_user', to: 'messages#mark_it_read_user'

  get 'welcome_screen', to: 'homepage#welcome_screen'

  get 'message_window', to: 'messages#message_window'
  get 'user_book_chat', to: 'messages#user_book_chat'
  get 'host_book_chat', to: 'messages#host_book_chat'

  post 'host_book_send_message', to: 'messages#host_book_send_message'
  post 'user_book_send_message', to: 'messages#user_book_send_message'

  get 'mark_as_important', to: 'messages#mark_as_important'
  get 'unmark_as_important', to: 'messages#unmark_as_important'

  # get 'dynamic_messages', to: 'messages#dynamic_messages'
  # post 'dynamic_messages', to: 'messages#dynamic_messages'

  get 'forget_password_form', to: 'password_resets#new'

  # get 'password_reset_form', to: 'password_resets#edit'

  # post 'update_password', to: 'password_resets#update'

  get 'edit_profile', to: 'dashboard#edit_profile_page'
  post 'update_profile', to: 'dashboard#update_profile'

  get 'new_book', to: 'book#new'
  get 'book_detail_page', to: 'book#book_details'
  get 'see_all_books', to: 'book#see_all_books'
  get 'request_book_details', to: 'book#request_book_details'
  post 'send_request_book_message', to: 'book#send_request_book_message'
  get 'see_all_external_books', to: 'book#see_all_external_books'
  get 'delete_book', to: 'book#delete_book'

  post 'add_book', to: 'book#add_book'
  get 'edit_book', to: 'book#edit_book_page'
  post 'update_book', to: 'book#update_book'
  get 'review', to: 'book#review'

  # get 'new_book_ad', to: 'book_ads#new'
  # post 'add_book_ad', to: 'book_ads#add_book_ad'

  post 'submit_feedback', to: 'feedback#submit_feedback'
  post 'submit_category', to: 'category#submit_category'

  resources :sessions, only: [:create]
  resources :users
  resources :password_resets
  resources :dashboard
  resources :password_resets
  resources :books
  # resources :books do
  #   collection do 
  #     get :autocomplete
  #   end
  # end
  # resources :book_ads
  resources :feedback
end
