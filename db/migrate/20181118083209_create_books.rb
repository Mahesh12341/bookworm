class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.references :user, foreign_key: true
      t.string :service_name
      t.text :description
      t.string :category
      t.string :manpower_rqd
      t.string :completion_time
      t.string :charging_type
      t.float :price
      t.string :service_status
      t.string :country_field
      t.string :state_field
      t.string :city_field
      t.string :availability_status
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
