class AddAttachmentImageToBookImages < ActiveRecord::Migration[5.2]
  def self.up
    change_table :book_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :book_images, :image
  end
end
