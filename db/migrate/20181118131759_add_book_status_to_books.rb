class AddBookStatusToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :book_status, :string
  end
end
