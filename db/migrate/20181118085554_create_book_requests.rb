class CreateBookRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :book_requests do |t|
      t.references :user, foreign_key: true
      t.references :book, foreign_key: true
      t.string :status
      t.boolean :mark_as_important_user
      t.boolean :mark_as_important_host

      t.timestamps
    end
  end
end
