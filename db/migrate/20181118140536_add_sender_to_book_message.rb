class AddSenderToBookMessage < ActiveRecord::Migration[5.2]
  def change
    add_column :book_messages, :sender, :string
  end
end
