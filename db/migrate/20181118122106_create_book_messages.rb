class CreateBookMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :book_messages do |t|
      t.references :book, foreign_key: true
      t.integer :sender_id
      t.integer :receipient_id
      t.boolean :host_is_read
      t.boolean :user_is_read
      t.text :message_description
      t.references :book_request, foreign_key: true

      t.timestamps
    end
  end
end
