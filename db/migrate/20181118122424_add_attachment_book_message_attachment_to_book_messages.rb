class AddAttachmentBookMessageAttachmentToBookMessages < ActiveRecord::Migration[5.2]
  def self.up
    change_table :book_messages do |t|
      t.attachment :book_message_attachment
    end
  end

  def self.down
    remove_attachment :book_messages, :book_message_attachment
  end
end
