class AddAboutYourselfToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :about_yourself, :text
  end
end
