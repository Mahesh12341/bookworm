class AddAcceptTermsColomnToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :terms_accepted, :boolean
  end
end
