class AddIsConfirmColomnToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_confirm, :boolean, default: false
  end
end
