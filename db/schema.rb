# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_18_140536) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "book_images", force: :cascade do |t|
    t.integer "book_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "book_messages", force: :cascade do |t|
    t.bigint "book_id"
    t.integer "sender_id"
    t.integer "receipient_id"
    t.boolean "host_is_read"
    t.boolean "user_is_read"
    t.text "message_description"
    t.bigint "book_request_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "book_message_attachment_file_name"
    t.string "book_message_attachment_content_type"
    t.integer "book_message_attachment_file_size"
    t.datetime "book_message_attachment_updated_at"
    t.string "sender"
    t.index ["book_id"], name: "index_book_messages_on_book_id"
    t.index ["book_request_id"], name: "index_book_messages_on_book_request_id"
  end

  create_table "book_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "book_id"
    t.string "status"
    t.boolean "mark_as_important_user"
    t.boolean "mark_as_important_host"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_book_requests_on_book_id"
    t.index ["user_id"], name: "index_book_requests_on_user_id"
  end

  create_table "books", force: :cascade do |t|
    t.bigint "user_id"
    t.string "service_name"
    t.text "description"
    t.string "category"
    t.string "manpower_rqd"
    t.string "completion_time"
    t.string "charging_type"
    t.float "price"
    t.string "service_status"
    t.string "country_field"
    t.string "state_field"
    t.string "city_field"
    t.string "availability_status"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "book_name"
    t.string "book_status"
    t.string "author"
    t.index ["category_id"], name: "index_books_on_category_id"
    t.index ["user_id"], name: "index_books_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "suggestion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.text "address"
    t.string "city"
    t.string "state"
    t.string "pincode"
    t.integer "phoneno"
    t.string "email"
    t.string "gender"
    t.string "password_hash"
    t.string "password_salt"
    t.string "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile_pic_file_name"
    t.string "profile_pic_content_type"
    t.integer "profile_pic_file_size"
    t.datetime "profile_pic_updated_at"
    t.string "auth_token"
    t.string "country_field"
    t.string "user_status"
    t.boolean "is_confirm", default: false
    t.boolean "terms_accepted"
    t.text "about_yourself"
  end

  add_foreign_key "book_messages", "book_requests"
  add_foreign_key "book_messages", "books"
  add_foreign_key "book_requests", "books"
  add_foreign_key "book_requests", "users"
  add_foreign_key "books", "categories"
  add_foreign_key "books", "users"
end
