class DashboardController < ApplicationController
	layout "user_dashboard_layout"


	def user_dashboard
		@user = User.find(session[:user_id])

		@my_books = Book.active.where(user_id: session[:user_id])
		# @book_ads = bookAd.where(user_id: session[:user_id])
		if @user.check_profile_complete_status(session[:user_id])
			@external_books = Book.where.not(user_id: session[:user_id]).active.available
		else
			@external_books = []
    end
	end

	def edit_profile_page
		@user = User.find(session[:user_id])
	end

	def update_profile
		@user = User.find(session[:user_id])
		if @user.update_attributes(user_params)
			flash[:success] = "The profile was updated successfully."
			redirect_to edit_profile_path
  	else
  		flash[:error] = "Error while updating Profile.."
  		redirect_to edit_profile_path
  	end	
	end


	def advanced_search_results
		# byebug
		@user = User.find(session[:user_id])
		if @user.check_profile_complete_status(session[:user_id])
			book_id_params =  Book.advance_search(session[:user_id],params[:book_name],params[:category],params[:city],params[:top_search_button],params[:bottom_search_button])
			# book_id_string = book_id_params.tr("/", ",")
			# book_id_array = book_id_string.split(",")
			# byebug
			@book = Book.where(id: book_id_params)
			@books = Book.where(id: book_id_params).paginate(:page => params[:page], :per_page => 10)
		else
			flash[:error] = "You are not authorised to Search Book since your profile is Incomplete,So complete it fast."
      redirect_to edit_profile_path
    end
	end

	def user_params
		params.require(:user).permit(User.attribute_names + [:profile_pic])
	end
	
end
