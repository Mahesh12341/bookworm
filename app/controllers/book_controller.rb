class BookController < ApplicationController
	layout "user_dashboard_layout"

	def new
		@book = Book.new
		@user = User.find(session[:user_id])
		@categories = Category.all
		if @user.check_profile_complete_status(session[:user_id])
		else
			flash[:error] = "You are not authorised to Add Book since your profile is Incomplete,So complete it fast."
      redirect_to edit_profile_path
    end
	end

	def add_book
		# byebug
		@user = User.find(session[:user_id])
		@book = Book.new(book_params)
		@book.book_status = "Active"
		@book.user_id = @user.id
		if @user.check_profile_complete_status(session[:user_id])
			if @book.save
				if params[:book_images]
	        params[:book_images].each { |book_image|
	          @book.book_images.create(image: book_image)
	        }
	 			end
					flash[:success] = "Book Added Successfully"
	    		redirect_to new_book_path
	    	else
	    		flash.now[:error] = "Error while Adding Book.."
	      	render action: 'new'
	    	end
	    else
	    	flash[:error] = "You are not authorised to Add Book since your profile is Incomplete,So complete it fast."
	      render action: 'new'
	    end
	end

	def edit_book_page
		@book = Book.find(params[:format])
		@categories = Category.all
	end

	def update_book
		@book = Book.find(params[:book_id])
		if @book.update_attributes(book_params)
			if params[:book_images]
        params[:book_images].each { |book_image|
          @book.book_images.create(image: book_image)
        }
 			end
			flash[:success] = "The Book was updated successfully."
			redirect_to :action => 'edit_book_page' , :format=> params[:book_id]
  	else
  		flash[:error] = "Error while updating Book."
			redirect_to :action => 'edit_book_page' , :format=> params[:book_id]
  	end	
	end

	# def autocomplete
 #    render json: book.search(params[:query], {
 #      fields: ["book_name"],
 #      match: :word_start,
 #      limit: 10,
 #      load: false,
 #      misspellings: {below: 5}
 #    }).map(&:book_name)
 #  end

	def book_details
		@book = Book.find(params[:format])
	end

	def see_all_books
		@books = Book.active.all.paginate(:page => params[:page], :per_page => 10)
	end

	def see_all_external_books
		# @external_books = book.where.not(user_id: session[:user_id]).active.available.paginate(:page => params[:page], :per_page => 10)
		@external_books = Book.includes(:category,:user).where.not(user_id: session[:user_id]).active.available.paginate(:page => params[:page], :per_page => 10)
	end

	def request_book_details
		@book = Book.find(params[:format])
		@particular_user_books = Book.active.available.where(user_id: @book.user_id)
	end

	def send_request_book_message
		# byebug
		puts params[:book_id]
		@book_request = BookRequest.new
		@book_request.book_id = params[:book_id]
		@book_request.user_id = session[:user_id]
		@book_request.status = "Open"
		if @book_request.save
			# byebug
			@book_message = BookMessage.new(message_description: params[:message_description],book_request_id: @book_request.id,sender: "user",sender_id: session[:user_id],receipient_id: @book_request.book.user_id,host_is_read: false,user_is_read: true)
			if params[:message_description].present?
				@book_message.save
				flash[:success] = "Book Requested Successfully"
    		redirect_to user_dashboard_path
    	else
    		flash[:error] = "Message can't be Blank"
    		redirect_to :action => 'request_book_details' , :format=> params[:book_id]
    	end
		end
	end

	def delete_book
		@book = Book.find(params[:format])
		@book.update(book_status: "Inactive")
		flash[:success] = "The Book was deleted successfully."
		redirect_to user_dashboard_path
	end


	def book_params
		params.require(:book).permit(Book.attribute_names)
	end
end
