class CategoryController < ApplicationController
	layout "user_dashboard_layout"

	def submit_category
		@category = Category.new(name: params[:name],description: params[:description])
		if @category.save
			flash[:success] = "Category Saved successfully."
			redirect_to new_book_path
		else
			flash[:error] = "Error occured while Saving category."
			redirect_to new_book_path
		end
	end

end
