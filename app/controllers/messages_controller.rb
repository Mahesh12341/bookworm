class MessagesController < ApplicationController
	layout "user_dashboard_layout"

	def message_window
		# byebug
		@user_ids = session[:user_id]
		@book=Book.where(:user_id => @user_ids).pluck(:id)

		@book_request=BookRequest.where(:book_id => @book).where.not(:user_id => @user_ids).pluck(:id)
		@HostBookMessages = BookMessage.select("DISTINCT ON (book_request_id) * ").where(book_request_id: @book_request).order("book_request_id, created_at DESC").sort_by &:created_at
		
		@user_book_request=BookRequest.where(:user_id => @user_ids).where.not(:book_id => @book).pluck(:id)
		@UserBookMessages = BookMessage.select("DISTINCT ON (book_request_id) * ").where(book_request_id: @user_book_request).order("book_request_id, created_at DESC").sort_by &:created_at

		@user = User.where(id: @user_ids).first
		if @user.check_profile_complete_status(session[:user_id])
		else
			flash[:error] = "You are not authorised to Send Message since your profile is Incomplete.So complete it fast."
      redirect_to edit_profile_path
    end
	end

	# def dynamic_messages
	# 	# byebug
	# 	@user_ids = session[:user_id]
	# 	@book=Book.where(:user_id => @user_ids).pluck(:id)

	# 	if params[:format] == "host"
	# 		@book_request=BookRequest.where(:book_id => @book).where.not(:user_id => @user_ids).pluck(:id)
	# 		@HostDynamicBookMessages = BookMessage.select("DISTINCT ON (book_request_id) * ").where(book_request_id: @book_request).order("book_request_id, created_at DESC").sort_by &:created_at
	# 		render json: nil, status: :ok
	# 	elsif params[:format] == "user"
	# 		# byebug
	# 		@user_book_request=BookRequest.where(:user_id => @user_ids).where.not(:book_id => @book).pluck(:id)
	# 		@UserDynamicBookMessages = BookMessage.select("DISTINCT ON (book_request_id) * ").where(book_request_id: @user_book_request).order("book_request_id, created_at DESC").sort_by &:created_at
	# 		render json: nil, status: :ok
	# 	end
	# 	# render json: nil, status: :ok
	# end


	def user_book_chat
		@user_ids = session[:user_id]
		@book=Book.where(:user_id => @user_ids).pluck(:id)
		@user_book_request=BookRequest.where(:user_id => @user_ids).where.not(:book_id => @book).pluck(:id)
		if @user_book_request.blank?
			redirect_to user_dashboard_path
		else
			if(@user_book_request.inspect.include? params[:format])
				@final_book_request=BookRequest.where(:id =>params[:format])
				# byebug
				@UserBookMessages = BookMessage.where(book_request_id: @final_book_request).order("created_at ASC")
				# @updated_messages = Array.new
				# @updated_messages  = AssetRequestMessages.where(asset_request_id: @final_asset_request.first.id,receipient_admin_id: session[:department_id],is_read: false).map{ |f| f.id }
				# @updated_ids = AssetRequestMessages.where(asset_request_id: @final_asset_request.first.id,receipient_admin_id: session[:department_id],is_read: false).update_all(is_read: true)
			end
			# render :nothing => true, :status => 200
			# render :layout => "iframe"
			render :layout => false
		end
	end


	def host_book_chat
		@user_ids = session[:user_id]
		@book=Book.where(:user_id => @user_ids).pluck(:id)
		@book_request=BookRequest.where(:book_id => @book).where.not(:user_id => @user_ids).pluck(:id)
		if @book_request.blank?
			redirect_to user_dashboard_path
		else
			if(@book_request.inspect.include? params[:format])
				@final_book_request=BookRequest.where(:id =>params[:format])
				@HostBookMessages = BookMessage.where(book_request_id: @final_book_request).order("created_at ASC")
				# @updated_messages  = BookRequestMessages.where(book_request_id: @final_book_request.first.id,receipient_admin_id: session[:department_id],is_read: false).map{ |f| f.id }
				# BookRequestMessages.where(book_request_id: @final_book_request.first.id,receipient_admin_id: session[:department_id],is_read: false).update_all(is_read: true)
			end
			# render :nothing => true, :status => 200
			# render :layout => "iframe"
			render :layout => false
		end
	end

	def host_book_send_message
		# byebug
		puts params[:book_request_id]
		@user_ids = session[:user_id]
		@book=Book.where(:user_id => @user_ids).pluck(:id)
		@book_request=BookRequest.where(:id => params[:book_request_id],:book_id => @book).where.not(:user_id => @user_ids)
		@book_messages=nil
		if(@book_request.present?)
			# byebug
			book_messages = BookMessage.new(message_description: params[:message_description],book_message_attachment: params[:book_message_attachment],book_request_id: params[:book_request_id],sender: "host",sender_id: session[:user_id],receipient_id: @book_request.first.user_id,host_is_read: true,user_is_read: false)
			book_messages.save
			# byebug
			BookMessage.where(receipient_id: @user_ids,book_request_id: params[:book_request_id],host_is_read: false).order("created_at ASC").update_all(host_is_read: true)
			UserMailer.send_email_to_user(book_messages.id).deliver
			# @book_messages=book_messages
		end
		flash[:success] = "Message To User Send Successfully"
		redirect_to message_window_path
		# redirect_to :action => 'host_book_chat' , :format=> params[:book_request_id] , :sender => 'host'
	end

	def	user_book_send_message
		@user_ids = session[:user_id]
		@book=Book.where(:user_id => @user_ids).pluck(:id)
		@book_request=BookRequest.where(:id => params[:book_request_id],:user_id => @user_ids).where.not(:book_id => @book)
		@book_messages=nil
		if(@book_request.present?)
			book_messages = BookMessage.new(message_description: params[:message_description],book_message_attachment: params[:book_message_attachment],book_request_id: params[:book_request_id],sender: "user",sender_id: session[:user_id],receipient_id: @book_request.first.Bookuser_id,host_is_read: false,user_is_read: true)
			book_messages.save
			BookMessage.where(receipient_id: @user_ids,book_request_id: params[:book_request_id],user_is_read: false).order("created_at ASC").update_all(user_is_read: true)
			UserMailer.send_email_to_provider(book_messages.id).deliver
			# @book_messages=book_messages
		end
		# render :nothing => true, :status => 200
		flash[:success] = "Message To Provider Send Successfully"
		redirect_to message_window_path
		# redirect_to :action => 'user_book_chat' , :format=> params[:book_request_id] , :sender => 'user'
	end

	def mark_as_important
		# byebug
		params[:sender]
		@book_request = BookRequest.find(params[:id])
		if params[:sender] == "host"
			@book_request.update(mark_as_important_host: true)
			render json: nil, status: :ok
		elsif params[:sender] == "user"
			@book_request.update(mark_as_important_user: true)
			render json: nil, status: :ok
		end
	end

	def unmark_as_important
		# byebug
		params[:sender]
		@book_request = BookRequest.find(params[:id])
		if params[:sender] == "host"
			@book_request.update(mark_as_important_host: false)
			render json: nil, status: :ok
		elsif params[:sender] == "user"
			@book_request.update(mark_as_important_user: false)
			render json: nil, status: :ok
		end
	end

	def mark_it_read_host
		# byebug
		BookMessage.where(receipient_id: session[:user_id],book_request_id: params[:format],host_is_read: false).update_all(host_is_read: true)
		flash[:success] = "All Message Send by User has been Marked as Read"
		redirect_to message_window_path
	end

	def mark_it_read_user
		BookMessage.where(receipient_id: session[:user_id],book_request_id: params[:format],user_is_read: false).update_all(user_is_read: true)
		flash[:success] = "All Message Send by Provider has been Marked as Read"
		redirect_to message_window_path
	end

end
