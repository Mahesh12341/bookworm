class FeedbackController < ApplicationController
	layout "user_dashboard_layout"

	def submit_feedback
		@feedback = Feedback.new(name: params[:name],email: params[:email],suggestion: params[:suggestion])
		if @feedback.save
			# UserMailer.send_suggestion_mail(@feedback.id).deliver
			flash[:success] = "Feedback Send successfully."
			redirect_to user_dashboard_path
		else
			flash[:error] = "Error occured while sending feedback."
			redirect_to user_dashboard_path
		end
	end


end
