class SessionsController < ApplicationController
  # skip_before_action :require_valid_user!, except: [:destroy]
  skip_before_action  except: [:destroy]

  def new
  end

	# def create
	#   user = User.find_by_email(params[:email])
	#   if user && user.authenticate(params[:password])
	#     if params[:remember_me]
	#       cookies.permanent[:auth_token] = user.auth_token
	#     else
	#       cookies[:auth_token] = user.auth_token
	#     end
	#     redirect_to root_url, :notice => "Logged in!"
	#   else
	#     flash.now.alert = "Invalid email or password"
	#     render "new"
	#   end
	# end

	# def destroy
	#   cookies.delete(:auth_token)
	#   redirect_to root_url, :notice => "Logged out!"
	# end


	def create
		reset_session
	  user = User.authenticate(params[:email], params[:password])
	  if user && user.is_confirm == true
	    session[:user_id] = user.id
	    flash[:success] = 'Welcome back!'
     	redirect_to user_dashboard_path
	  else
	  	if user && user.is_confirm == false
	  		flash[:error] = 'Account not yet confirmed so you cannot login, Check your email for confirmation link'
      	redirect_to login_path
      else
		    flash[:error] = 'Invalid email/password combination'
	      redirect_to login_path
	     end
	  end
	end

	def destroy
	  reset_session
	end

	def session_params
    	params.require(:session).permit(:email, :password)
	end
end
