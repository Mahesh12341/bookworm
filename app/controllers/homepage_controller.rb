class HomepageController < ApplicationController
	layout "homepage_layout"

	def index

	end

	def welcome_screen
		@welcome_external_books = Book.where.not(user_id: session[:user_id]).active.available
	end

end
