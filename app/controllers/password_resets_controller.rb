class PasswordResetsController < ApplicationController
  # before_action :get_user,         only: [:edit, :update]
  # # before_action :valid_user,       only: [:edit, :update]
  # before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
  	user = User.find_by(email: params[:password_reset][:email])
  	if user
  		user.send_password_reset_email
  		redirect_to login_path, :notice => "Email sent with password reset instructions."
  	else
  		flash[:error] = "Email address not found or blank"
      redirect_to forget_password_form_path
  	end
	end

	# def edit
	#   @user = User.find(params[:id])
	# end

	# def update
	#   @user = User.find_by_password_reset_token!(params[:password_reset_token])
 #    if params[:user][:password].empty?               # Case (3)
 #      flash[:error] = "Password can't be empty."
 #      redirect_to password_reset_form_url(@user.password_reset_token,id: @user.id,email: @user.email)
 #    elsif @user.password_reset_token.blank? && @user.password_reset_sent_at.present?
 #      flash[:error] = "Password reset link has been used previously."
 #      redirect_to login_path
	#   elsif @user.update_attributes(user_params)
	#   	@user.update(password_reset_token: '')
	#     redirect_to login_path, :notice => "Password has been reset!"
	#   else
	#     render :edit
	#   end
	# end

  def edit
    @user = User.find_by_password_reset_token!(params[:id])
  end

  def update
    @user = User.find_by_password_reset_token!(params[:id])
    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, :alert => "Password reset has expired."
    elsif @user.update_attributes(user_params)
      redirect_to login_path, :notice => "Password has been reset!"
    else
      render :edit
    end
  end

	# def update
 #    # byebug
 #    @user = User.find_by(email: params[:email])
 #    if params[:user][:password].empty?               # Case (3)
 #      flash[:error] = "Password can't be empty."
 #      redirect_to password_reset_form_url(@user.password_reset_token,id: @user.id,email: @user.email)
 #    elsif @user.password_reset_token.blank?                 # Case (3)
 #      flash[:error] = "Password reset link has been used previously."
 #      redirect_to login_path
 #    elsif @user.update_attributes(user_params)          # Case (4)
 #      @user.update(password_reset_token: '')
 #      flash[:success] = "Password has been reset."
 #      redirect_to login_path
 #    else
 #      flash[:error] = "Error Occured during Reset."
 #      redirect_to login_path                                   # Case (2)
 #    end
 #  end

  private

    def user_params
      params.require(:user).permit(User.attribute_names)
    end

    # Before filters

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirms a valid user.
    # def valid_user
    #   unless (@user &&
    #           @user.authenticated?(:reset, params[:id]))
    #     redirect_to root_url
    #   end
    # end

    # Checks expiration of reset token.
    # def check_expiration
    #   if @user.password_reset_expired?
    #     flash[:error] = "Password reset has expired."
    #     redirect_to new_password_reset_url
    #   end
    # end

end
