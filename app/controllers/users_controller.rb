class UsersController < ApplicationController
  # skip_before_action :require_valid_user!
  before_action :reset_session

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      session[:user_id] = @user.id
      UserMailer.send_register_email(@user.id).deliver
      flash.now[:success] =  'You may have received confirmation link in email,Please click on that to complete the Registration process!'
      render action: 'new'
    else
      flash.now[:error] =  'Error occured during Registration'
      render action: 'new'
    end
  end

  def confirm_email
    @user = User.find_by_email(params[:email])
    if @user.present? && @user.is_confirm == false
      @user.update(is_confirm: true,user_status: 'Active')
      UserMailer.confirmation_email(@user.id).deliver
      flash.now[:success] =  'Your email address has been successfully Confirmed,please Sign in'
      render action: 'confirmation_page'
    else
      if @user.is_confirm == true
        flash.now[:success] =  'Account already confirmed,proceed to login.'
        render action: 'confirmation_page'
      else
        flash.now[:error] =  'Your email is not present in our database'
        render action: 'confirmation_page'
      end
    end
  end

  def confirmation_page

  end

  def user_params
    params.require(:user).permit(User.attribute_names + [:password, :password_confirmation])
  end
end