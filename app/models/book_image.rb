class BookImage < ActiveRecord::Base
  belongs_to :book

  has_attached_file :image, :styles => {:thumb => "200x250" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
