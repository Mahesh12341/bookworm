class User < ActiveRecord::Base  
  attr_accessor :password
  before_save :encrypt_password

  has_attached_file :profile_pic, :styles => {:thumb => "200x250>" }
  validates_attachment_content_type :profile_pic, :content_type => /\Aimage\/.*\Z/
  # validates_presence_of :first_name,:city,:state,:country_field,:email
  
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email,:terms_accepted,:first_name,:last_name
  validates_uniqueness_of :email
  has_many :books
  scope :active, -> { where user_status: "Active" }
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def send_password_reset_email
  	generate_token(:password_reset_token)
  	self.password_reset_sent_at = Time.zone.now
  	save!
  	UserMailer.password_reset(self).deliver
	end

	def generate_token(column)
  	begin
    	self[column] = SecureRandom.urlsafe_base64
  	end while User.exists?(column => self[column])
	end

  def password_reset_expired?
    reset_sent_at < 10.minutes.ago
  end

  def name
    self.first_name + " "+ self.last_name
  end

  def profile_complete_status(user_id)
    @user = User.where(id: user_id).first
    if (@user.first_name.blank? || @user.last_name.blank? || @user.country_field.blank? || @user.state.blank? || @user.city.blank? || @user.about_yourself.blank?)
      return "<i title='Incomplete Profile' class='fas fa-exclamation-triangle fa-1x' style='color:#FE6E00;position: absolute;top: 8px;right: 177px;'></i>"
    else
      return ''
    end
  end

  def edit_profile_complete_status_front(user_id)
    @user = User.where(id: user_id).first
    if(@user.first_name.blank? ||  @user.last_name.blank? || @user.country_field.blank? || @user.state.blank? || @user.city.blank? || @user.about_yourself.blank?)
      return "<i title='Incomplete Profile' class='fas fa-exclamation-triangle fa-2x' style='color: #FE6E00;position: absolute;right: 588px;'></i>"
    else
      return ''
    end
  end


  def edit_profile_complete_status_modal(user_id)
    @user = User.where(id: user_id).first
    if(@user.first_name.blank? || @user.last_name.blank? || @user.country_field.blank? || @user.state.blank? || @user.city.blank? || @user.about_yourself.blank?)
      return "<i title='Incomplete Profile' class='fas fa-exclamation-triangle fa-2x' style='color: #FE6E00;position: absolute;right: 282px;'></i>"
    else
      return ''
    end
  end

  def check_profile_complete_status(user_id)
    @user = User.where(id: user_id).first
    if(@user.first_name.blank? ||  @user.last_name.blank? || @user.country_field.blank? || @user.state.blank? || @user.city.blank? || @user.about_yourself.blank?)
      return false
    else
      return true
    end
  end

  def user_location_formating
    if self.country_field.present? && self.state.present? && self.city.present? 
      self.country_field + '(' + self.state + ',' + self.city + ')'
    else
      ''
    end
  end

end