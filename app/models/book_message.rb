class BookMessage < ActiveRecord::Base
  belongs_to :book_request
  has_attached_file :book_message_attachment
  validates_attachment :book_message_attachment, :content_type => {:content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)}

  def book_message_sender_name
  	@user = User.find(sender_id)
  	if self.sender_id == self.book_request.book.user_id
  		return "<a href='#' title='Owner of book' style='text-decoration:none;color:#212529;'>" << @user.first_name << " "  << @user.last_name << "&nbsp;<i class='fa fa-user fa-1x' aria-hidden='true'></i></a>"
		elsif self.sender_id == self.book_request.user_id
			return @user.first_name << " "  << @user.last_name 
		end
  end

	def book_request_sender_type(session_user_id)
		if self.book_request.book.user_id == session_user_id
			return "&nbsp;<i title='Owner of book' style='text-decoration:none;color:#212529;' class='fa fa-user fa-1x' aria-hidden='true'></i>"
		elsif self.book_request.user_id == session_user_id
			return ''
		end
  end

  def self.unread_message_count_for_span(user_id)
  	@book_request = BookRequest.where(status: "Open").pluck(:id)
  	# byebug
		@provider_unread_message_count = BookMessage.where(book_request_id: @book_request,receipient_id: user_id,host_is_read: false).count
		@user_unread_message_count = BookMessage.where(book_request_id: @book_request,receipient_id: user_id,user_is_read: false).count  	
		total = @provider_unread_message_count + @user_unread_message_count
		# byebug
		if total > 0
			if total < 9
				# byebug
				return "<span id='badge-notify' class='badge'>" << total.to_s << "</span>"
			elsif total > 9
				return "<span id='badge-notify' class='badge'>9+</span>"
			end
		else
			return ''
		end
  end

  def self.unread_message_count(user_id)
  	@book_request = BookRequest.where(status: "Open").pluck(:id)
		@provider_unread_message_count = BookMessage.where(book_request_id: @book_request,receipient_id: user_id,host_is_read: false).count
		@user_unread_message_count = BookMessage.where(book_request_id: @book_request,receipient_id: user_id,user_is_read: false).count  	
		total = @provider_unread_message_count + @user_unread_message_count
		if total > 0
			return total
		else
			return "0"
		end
  end

  def particular_user_unread_message_count(user_id)
		@user_unread_message_count = BookMessage.where(book_request_id: self.book_request_id,receipient_id: user_id,user_is_read: false).count  	
		if @user_unread_message_count > 0
			return true
		else
			return false
		end
  end

  def particular_provider_unread_message_count(user_id)
		@provider_unread_message_count = BookMessage.where(book_request_id: self.book_request_id,receipient_id: user_id,host_is_read: false).count
		if @provider_unread_message_count > 0
			return true
		else
			return false
		end
  end

end
