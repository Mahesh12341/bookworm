class Book < ActiveRecord::Base
	# searchkick
  belongs_to :user
  belongs_to :category
  has_many :book_images
  has_many :book_messages
  has_many :book_requests
  validates_presence_of :author,:book_name,:description,:category_id,:country_field,:state_field,:city_field,:price,:availability_status
  scope :active, -> { where(book_status: ["Active"]) }
  scope :available, -> { where availability_status: "Available" }

	def get_default_image_for_show_page
		if !self.book_images.blank?
			self.book_images.first.image.url(:thumb)	
		else
			return ActionController::Base.helpers.asset_path("default-slideshow-image.jpg")
		end
	end

	def get_default_image_for_index_page
		if !self.book_images.blank?
			self.book_images.first.image.url(:thumb)	
		else
			return ActionController::Base.helpers.asset_path("default-slideshow-image.jpg")
		end
	end

	def book_requested_before
		# byebug
		@book_requests = BookRequest.where(book_id: self.id).count > 0 ? true : false
		return @book_requests
	end

	def location_formating
		self.country_field + '(' + self.state_field + ',' + self.city_field + ')'
	end


	def self.advance_search(session_user_id,book_name,category,city,top_search,bottom_search)

		@my_books = Book.where.not(user_id: session_user_id).active.available.pluck(:id)
		search = book_name.present? ? book_name : nil
		if top_search
			# byebug
			if search
				@book_ids = Book.where("book_name LIKE ?", "%#{search}%").where.not(user_id: session_user_id).active.available.pluck(:id)
			else
				@book_ids = @my_books
			end
		elsif bottom_search
			# byebug
			if search && category.present? && city.blank?
				@category = Category.where("name LIKE ?", "%#{category}%").pluck(:id)
				@book_ids = Book.where("book_name LIKE ?", "%#{search}%").where(category_id: @category).where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search && category.blank? && city.blank?
				@book_ids = Book.where("book_name LIKE ?", "%#{search}%").where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.blank? && category.present? && city.blank?	
				@category = Category.where("name LIKE ?", "%#{category}%").pluck(:id)
				@book_ids = Book.where(category_id: @category).where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.present? && category.blank? && city.present?	
				@book_ids = Book.where("book_name LIKE ? AND city_field LIKE ?", "%#{search}%","%#{city}%").where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.blank? && category.present? && city.present?	
				@category = Category.where("name LIKE ?", "%#{category}%").pluck(:id)
				@book_ids = Book.where("city_field LIKE ?","%#{city}%").where(category_id: @category).where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.blank? && category.blank? && city.present?	
				@book_ids = Book.where("city_field LIKE ?","%#{city}%").where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.present? && category.present? && city.present?	
				@category = Category.where("name LIKE ?", "%#{category}%").pluck(:id)
				@book_ids = Book.where("book_name LIKE ? AND city_field LIKE ?", "%#{search}%","%#{city}%").where(category_id: @category).where.not(user_id: session_user_id).active.available.pluck(:id)
			elsif search.blank? && category.blank?	&& city.blank?
				@book_ids = @my_books
			else
				@book_ids = []
			end
		end
		return @book_ids
	end


end
