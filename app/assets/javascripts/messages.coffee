# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('.host-book-inbox-link').on 'click', (e) ->
    e.preventDefault()
    $('.msg-box-loader').show()
    $.ajax
      url: $(this).attr('href');
      type: 'get'
      cache: false
      success: (res) ->
        $('.msg-box').html(res)
        $('#header-profile-image').attr 'src', $('input[name="user_profile_pic"]').val()
        $('#header-user-name').html($('input[name="sender_name"]').val())
        $('.host-send-message-footer').show()
        $(".msg-box-loader").hide()
      error: ->
        alert 'Error While loading Messages'

  $('.user-book-inbox-link').on 'click', (e) ->
    e.preventDefault()
    $('.msg-box-loader').show()
    $.ajax
      url: $(this).attr('href');
      type: 'get'
      cache: false
      success: (res) ->
        $('.msg-box').html(res)
        $('#header-profile-image').attr 'src', $('input[name="provider_profile_pic"]').val()
        $('#header-user-name').html($('input[name="sender_name"]').val())
        $('.user-send-message-footer').show()
        $(".msg-box-loader").hide()
      error: ->
        alert 'Error While loading Messages'

  $('.unstarred').on 'click', (e) ->
    e.preventDefault()
    $.ajax
      url: $(this).attr('href');
      type: 'get'
      cache: false
      success: (res) ->
        # alert 'success'
        $('.unstarred').hide()
        $('.starred').show()
        toastr.info 'Marked as Important'
      error: ->
        alert 'Error While Marking it Important'

  $('.starred').on 'click', (e) ->
    e.preventDefault()
    $.ajax
      url: $(this).attr('href');
      type: 'get'
      cache: false
      success: (res) ->
        # alert 'success'
        $('.starred').hide()
        $('.unstarred').show()
        toastr.warning 'Unmarked as Important!'
      error: ->
        alert 'Error While Unmarking it Important'

  # $('#search_by_msg_type').change ->
  #   $('.dynamic-msg-box-loader').show()
  #   if $('#search_by_msg_type').val() == "host"
  #     dyn_url = '/dynamic_messages.'+'host'
  #   else if $('#search_by_msg_type').val() == "user"
  #     dyn_url = '/dynamic_messages.'+'user'
  #   $.ajax
  #     url: dyn_url;
  #     type: 'get'
  #     cache: false
  #     success: (res) ->
  #       alert 'success'
  #       alert res
  #       $('#inbox-notify-card').hide()
  #       $('.dynamic-card').html(res)
  #       # $('#dynamic-inbox-notify-card').show()
  #       $(".dynamic-msg-box-loader").hide()
  #     error: ->
  #       alert 'Error While Displaying Message Inbox'

  # $('#user_message_form').on 'submit', (e) ->
  #   alert 'form click'
  #   e.preventDefault()
  #   valuesToSubmit = $(this).serialize()
  #   $.ajax
  #     type: 'POST'
  #     url: (this).attr('action')
  #     success: (res) ->
  #       alert 'success'
  #       ('.abc').html('<p>AAAAAAAAAAAAAAAAAAAAAAAAAAA</p>')
  #     error: ->
  #       alert 'Error While loading Messages'

  # $('#send_msg_btn').prop('disabled', true)

  # success = ->
  #   if $('#message_description').val() == ''
  #     $('#send_msg_btn').prop('disabled', true)
  #   else
  #     $('#send_msg_btn').prop('disabled', false)
  #   return

  # $('#message_description').keyup ->
  #   alert 'aaaaaaaaaaaaaaaaa'
  #   success()
  #   return

  # $('.list-group-item').on 'click', (e) ->
  #   $(this).addClass 'active'

  

  $('.message-inbox-link').click()

  # messageBody = document.querySelector('.message-box')
  # messageBody.scrollTop = messageBody.scrollHeight - (messageBody.clientHeight)
  
  # url = 'localhost:3000/unread_message_count'
  # alert url
