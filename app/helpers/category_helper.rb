module CategoryHelper
  def all_category
    Category.all.collect { |x| [x.name, x.id] }
  end

  def all_category_without_id
    Category.all.collect { |x| [x.name] }
  end

end
