class UserMailer < ApplicationMailer

	def password_reset(user)
  	@user = user
  	mail to: user.email, subject: "Password reset"
	end

	def send_email_to_provider(id)
		begin
			@book_message = BookMessage.where(id: id).first
			@user_detail = User.where(id: @book_message.book_request.user_id).first
			@provider_detail = User.where(id: @book_message.book_request.book.user_id).first
			mail(to: @provider_detail.email, subject: "New Message from User <%= @user_detail.user.name %>")
		rescue Exception => excep
			logger.error("Error Message #{excep.message}")
		end
	end

	def send_email_to_user(id)
		begin
			@book_message = BookMessage.where(id: id).first
			@user_detail = User.where(id: @book_message.book_request.user_id).first
			@provider_detail = User.where(id: @book_message.book_request.book.user_id).first
			mail(to: @user_detail.email, subject: "New Message from Provider <%= @provider_detail.user.name %>")
		rescue Exception => excep
			logger.error("Error Message #{excep.message}")
		end
	end

	def send_register_email(id)
		begin
			@user = User.where(id: id).first
			mail(to: @user.email, subject: "New Registration Request")
		rescue Exception => excep
			logger.error("Error Message #{excep.message}")
		end
	end

	def confirmation_email(id)
		begin
			@user = User.where(id: id).first
			mail(to: @user.email, subject: "Confirmation Completed")
		rescue Exception => excep
			logger.error("Error Message #{excep.message}")
		end
	end

	# def send_suggestion_mail(id)
	# 	begin
	# 		@feedback = Feedback.where(id: id).first
	# 		mail(to: @feedback.email, subject: "Suggestion Mail")
	# 	rescue Exception => excep
	# 		logger.error("Error Message #{excep.message}")
	# 	end
	# end

end
